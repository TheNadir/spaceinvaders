
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Nikhil Sinha sinhanr@mail.uc.edu
 */
public class Img extends Rectangle {
    
    //Global Variables
    private Image image;
    
    
    
    public Img(int x, int y, int width, int height, Image newImage) {
        super(x, y, width, height);
        image = newImage;
    }
    
    public Img (int x, int y, int width, int height, String imgPath) {
        super(x, y, width, height);
        
        ImageIcon ii = new ImageIcon(this.getClass().getResource(imgPath));
        image = ii.getImage();
    }
    
    
    @Override
    public void draw(Graphics g) {
        g.drawImage(getImage(), getX(), getY(), getWidth(), getHeight(), null);
    }
    
    
    
    //Getters and Setters
    public Image getImage() {
        return image;
    }
    public void setImage(Image newImage) {
        image = newImage;
    }
    public void setImageByPath(String newPath) {
        ImageIcon ii = new ImageIcon(this.getClass().getResource(newPath));
        image = ii.getImage();
    }
}
