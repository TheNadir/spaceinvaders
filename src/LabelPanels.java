
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.io.File;
import java.io.IOException;
import javax.swing.JLabel;
import javax.swing.JPanel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Nikhil Sinha sinhanr@mail.uc.edu
 */
public class LabelPanels extends JPanel {
    
    //Global Variables
    private JPanel topPanel, bottomPanel;
    private JLabel scoreLbl, levelLbl;
    
    
    
    public LabelPanels() {
        initTopPanel();
        initBottomPanel();
    }
    
    
    private void initTopPanel() {
        topPanel = new JPanel();
        topPanel.setLayout(new GridLayout(2, 2));
        Font font = new Font("Fixedsys Regular", Font.BOLD, 25);
        
        /*try {
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("fonts/PressStart2P.ttf")));
            font = Font.createFont(Font.TRUETYPE_FONT, new File("fonts/PressStart2P.ttf"));
        } catch (IOException ex) {
            System.out.println("ERROR");
            ex.printStackTrace();
        } catch (FontFormatException ex) {
            System.out.println("ERROR");
            ex.printStackTrace();
        }
        */
        
        
        scoreLbl = new JLabel("0");
        scoreLbl.setFont(font);
        scoreLbl.setForeground(Color.WHITE);
        levelLbl = new JLabel("0");
        levelLbl.setFont(font);
        levelLbl.setForeground(Color.WHITE);
        JLabel scoreTitleLbl = new JLabel("SCORE: ");
        scoreTitleLbl.setFont(font);
        scoreTitleLbl.setForeground(Color.WHITE);
        JLabel levelTitleLbl = new JLabel("LEVEL: ");
        levelTitleLbl.setFont(font);
        levelTitleLbl.setForeground(Color.WHITE);
        
        topPanel.add(scoreTitleLbl);
        topPanel.add(scoreLbl);
        topPanel.add(levelTitleLbl);
        topPanel.add(levelLbl);
    }
    
    private void initBottomPanel() {
        bottomPanel = new JPanel();
    }
    
    public JPanel getTopPanel() {
        return topPanel;
    }
    
    public JPanel getBottomPanel() {
        return bottomPanel;
    }
    
    public void setScore(int newScore) {
        scoreLbl.setText(String.valueOf(newScore));
        scoreLbl.updateUI();
    }
    
    public void setLevel(int newLevel) {
        levelLbl.setText(String.valueOf(newLevel));
        levelLbl.updateUI();
    }
    
}
