
import java.awt.Color;
import java.awt.Graphics;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Nikhil Sinha sinhanr@mail.uc.edu
 */
public class Shields extends Element {
    
    
    /**
     * width to CELL_WIDTH -> 54:2
     * height to CELL_HEIGHT -> 54:2
     */
    
    
    //Global Variables
    private int CELL_WIDTH = 2;
    private int CELL_HEIGHT = 2;
    private final int TOTAL_WIDTH = 36;
    private final int TOTAL_HEIGHT = 36;
    
    private boolean[][] cells;
    private Rectangle[][] rects;

    
    
    
    
    public Shields(int x, int y, int width, int height, Color color) {
        super(x, y, width, height, color);
        
        //Set the CELL_WIDTH and CELL_HEIGHT to the correct 52:2 ratio
        CELL_WIDTH = (2 * getWidth()) / 54;
        CELL_HEIGHT = (2 * getHeight()) / 54;
        
        cells = new boolean[36][36];
        rects = new Rectangle[36][36];
        
        createShields();
    }
    
    
    
    
    public void createShields() {
        for (int row = 0; row < rects.length; row ++) {
            for (int col = 0; col < rects[0].length; col ++) {
                rects[row][col] = null;
                cells[row][col] = false;
            }
        }
        
        for (int row = 0; row < rects[0].length; row ++) {
            if (row >= 8 && row <= 27) {
                cells[row][0] = true;
                cells[row][1] = true;
            }
            if (row >= 7 && row <= 28) {
                cells[row][2] = true;
                cells[row][3] = true;
            }
            if (row >= 6 && row <= 29) {
                cells[row][4] = true;
                cells[row][5] = true;
            }
            if (row >= 5 && row <= 30) {
                cells[row][6] = true;
                cells[row][7] = true;
            }
            if (row >= 4 && row <= 31) {
                cells[row][8] = true;
                cells[row][9] = true;
            }
            if (row >= 3 && row <= 32) {
                cells[row][10] = true;
                cells[row][11] = true;
            }
            if (row >= 2 && row <= 33) {
                cells[row][12] = true;
                cells[row][13] = true;
            }
            if (row >= 1 && row <= 34) {
                cells[row][14] = true;
                cells[row][15] = true;
            }
            
            for (int col = 16; col < 32; col ++) {
                cells[row][col] = true;
            }
            
            if (row <= 12 || row >= 23) {
                cells[row][32] = true;
            }
            if (row <= 11 || row >= 24) {
                cells[row][33] = true;
            }
            if (row <= 10 || row >= 25) {
                cells[row][34] = true;
            }
            if (row <= 9 || row >= 26) {
                cells[row][35] = true;
            }
        }
        
        for (int row = 0; row < rects.length; row ++) {
            for (int col = 0; col < rects.length; col ++) {
                if (cells[row][col]) {
                    rects[row][col] = new Rectangle(getX()+(row * CELL_WIDTH), getY()+(col * CELL_HEIGHT), CELL_WIDTH, CELL_HEIGHT, getColor());
                }
            }
        }
    }
    
    public void update() {
        for (int row = 0; row < rects.length; row ++) {
            for (int col = 0; col < rects[0].length; col ++) {
                if (rects[row][col] != null) {
                    cells[row][col] = true;
                }
                else {
                    cells[row][col] = false;
                }
            }
        }
    }
    
    
    @Override
    public void draw(Graphics g) {
        for (int row = 0; row < cells.length; row ++) {
            for (int col = 0; col < cells[0].length; col ++) {
                if (cells[row][col]) {
                    rects[row][col].draw(g);
                }
            }
        }
        update();
    }

    @Override
    public boolean isColliding(Element e) {
        for (int row = 0; row < cells.length; row ++) {
            for (int col = 0; col < cells[0].length; col ++) {
                if (rects[row][col] != null) {
                    if (e.isColliding(rects[row][col])) {
                        //destroy(row, col);
                        destroy(row, col, 2);
                        return true;
                    }
                }
            }
        }
        
        return false;
    }
    
    /**
     * check if the cell [row][col] is a valid shield cell
     * @param row - row to check
     * @param col - col to check
     * @return true if [row][col] is a valid shield cell
     */
    public boolean cellExists(int row, int col) {
        if (row >= 0 && row < TOTAL_HEIGHT && col >= 0 && col < TOTAL_WIDTH) {
            if (cells[row][col]) {
                return true;
            }
        }
        
        return false;
    }
    
    public void destroy(int r, int c) {
        for (int row = r - 2; row <= r + 2; row ++) {
            for (int col = c - 2; col <= c + 2; col ++) {
                if (cellExists(row, col)) {              //in the borders
                    rects[row][col] = null;
                    cells[row][col] = false;
                }
            }
        }
        
        
        //Scatter
        boolean tf = false;
        
        for (int row = r - 3; row <= r + 3; row ++) {
            for (int col = c - 3; col <= c + 3; col ++) {
                tf = !tf;
                if (cellExists(row, col)) {              //in the borders
                    if (tf) {
                        rects[row][col] = null;
                        cells[row][col] = false;
                    }
                }
            }
        }
    }
    
    public void destroy(int row, int col, int degree) {
        if (cellExists(row, col)) {
            rects[row][col] = null;
            cells[row][col] = false;
        }
        else {                          //cell hit doesn't exists - some error occured
            return;
        }
        
        for (int r = row - degree; r <= row + degree; r ++) {
            for (int c = col - degree; c <= col + degree; c ++) {
                if (cellExists(r, c)) {              //in the borders
                    rects[r][c] = null;
                    cells[r][c] = false;
                }
            }
        }
        
        
        //Scatter
        boolean tf = false;
        double percent = 0.0;
        
        for (int r = row - degree - 1; r <= row + degree + 1; r ++) {
            for (int c = col - degree - 1; c <= col + degree + 1; c ++) {
                tf = !tf;
                if (cellExists(r, c)) {              //in the borders
                    if (tf) {
                        percent = getRandDouble();
                        if (percent < 0.80) {               //80% chance of dammaging cell
                            rects[r][c] = null;
                            cells[r][c] = false;
                        }
                    }
                }
            }
        }
    }
    
}
