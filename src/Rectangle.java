
import java.awt.Color;
import java.awt.Graphics;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Nikhil Sinha sinhanr@mail.uc.edu
 */
public class Rectangle extends Element {
    
    public Rectangle(int x, int y, int width, int height, Color color) {
        super(x, y, width, height, color);
    }
    
    public Rectangle(int x, int y, int width, int height) {
        super(x, y, width, height);
    }
    
    
    @Override
    public void draw(Graphics g) {
        g.setColor(getColor());
        g.fillRect(getX(), getY(), getWidth(), getHeight());
    }
}
