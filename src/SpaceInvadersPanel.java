
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Random;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Nikhil Sinha sinhanr@mail.uc.edu
 */
public class SpaceInvadersPanel extends JPanel implements Runnable {
    
    
    //Global Variables
    private final static int WIDTH = 1000;
    private final static int HEIGHT = 600;
    private final double FPS = 10;
    private final int CHARACTER_WIDTH = 43;                 //4     original = 53
    private final int CHARACTER_HEIGHT = 30;                //3     original = 40
    private final int SHIELD_WIDTH = 100;                   //5
    private final int SHIELD_HEIGHT = 60;                   //3
    private final static int MISSILE_WIDTH = 3;
    private final static int MISSILE_HEIGHT = 30;
    private final static int ENEMY_MISSILE_WIDTH = 5;
    private final static int ENEMY_MISSILE_HEIGHT = 5;
    private final int MISSILE_MOVE = 6;
    private final static int MAX_MISSILE_COUNT = 2;
    
    private static Collection<Element> elements = new ArrayList<>();
    private Rectangle background;
    private Rectangle[] outerWalls = new Rectangle[4];
    private static Player player;
    private ArrayList<ArrayList<Enemy>> enemyRows = new ArrayList<>();
    private static ArrayList<Rectangle> missiles = new ArrayList<>();
    private ArrayList<Rectangle> enemyMissiles = new ArrayList<>();
    private Enemy bomber = null;                                    //null when bomber doesn't exist
    
    private Shields[] shields = new Shields[4];
    
    private int timer = 0;
    private int enemyMissileTimer = 0;
    private int nextEnemyFire = getRandomNum(0, 95);
    private int bomberTimer = getRandomNum(0, 1000);
    
    private int level = 1;
    private int score = 0;
    private int lives = 3;
    
    
    
    
    public static void main(String[] args) {
        JFrame parentWindow = new JFrame("Space Invaders");
        LabelPanels descriptionPanels = new LabelPanels();
        
        parentWindow.setLayout(new BorderLayout());
        
        parentWindow.getContentPane().add(new SpaceInvadersPanel(), BorderLayout.CENTER);
        parentWindow.getContentPane().add(descriptionPanels.getTopPanel(), BorderLayout.NORTH);
        
        parentWindow.addKeyListener(new keyListener());
        parentWindow.addKeyListener(player.getListener());
        
        parentWindow.setSize(WIDTH+15, HEIGHT+39+45);
        parentWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        parentWindow.setLocationRelativeTo(null);
        parentWindow.setVisible(true);
    }
    
    
    public SpaceInvadersPanel() {
        elements.add(background = new Rectangle(0, 0, WIDTH, HEIGHT, Color.BLACK));
        elements.add(outerWalls[0] = new Rectangle(-1, 0, 1, HEIGHT, Color.RED));                  //Left wall
        elements.add(outerWalls[1] = new Rectangle(WIDTH, 0, 1, HEIGHT, Color.RED));               //Right wall
        elements.add(outerWalls[2] = new Rectangle(0, -1, WIDTH, 1, Color.RED));                   //Top wall
        elements.add(outerWalls[3] = new Rectangle(0, HEIGHT, WIDTH, 1, Color.RED));               //Bottom wall
        elements.add(player = new Player(WIDTH/2-50, HEIGHT-80, CHARACTER_WIDTH, CHARACTER_HEIGHT, "images/Player.png"));
        /*for (int i = 0; i < 8; i ++) {
            enemies.add(new Enemy(i * (CHARACTER_WIDTH+30), 10, CHARACTER_WIDTH, CHARACTER_HEIGHT, "images/Enemy1.1.png"));
        }*/
        //Enemy Creation
        for (int i = 0; i < 5; i ++) {
            enemyRows.add(new ArrayList<Enemy>());
        }
        for (int row = 0; row < enemyRows.size(); row ++) {
            for (int col = 0; col < 10; col ++) {
                if (row < 1) {
                    enemyRows.get(row).add(new Enemy(col * (CHARACTER_WIDTH+30), (row * (CHARACTER_HEIGHT+10) + 60), 
                                       CHARACTER_WIDTH, CHARACTER_HEIGHT, "images/Enemy3.1.png"));
                }
                else if (row < 3) {
                    enemyRows.get(row).add(new Enemy(col * (CHARACTER_WIDTH+30), (row * (CHARACTER_HEIGHT+10) + 60), 
                                       CHARACTER_WIDTH, CHARACTER_HEIGHT, "images/Enemy2.1.png"));
                }
                else {
                    enemyRows.get(row).add(new Enemy(col * (CHARACTER_WIDTH+30), (row * (CHARACTER_HEIGHT+10) + 60), 
                                           CHARACTER_WIDTH, CHARACTER_HEIGHT, "images/Enemy1.1.png"));
                }
            }
        }
        //Shield Creation
        int spacing = (WIDTH - (4 * SHIELD_WIDTH)) / 5;             //5a - 4b = 1000; 5(spacing) - 4(SHIELD_WIDTH) = WIDTH
        elements.add(shields[0] = new Shields(spacing, HEIGHT-180, SHIELD_WIDTH, SHIELD_HEIGHT, Color.GREEN));
        elements.add(shields[1] = new Shields(spacing+SHIELD_WIDTH+spacing, HEIGHT-180, SHIELD_WIDTH, SHIELD_HEIGHT, Color.GREEN));
        elements.add(shields[2] = new Shields(spacing+SHIELD_WIDTH+spacing + SHIELD_WIDTH+spacing, HEIGHT-180, SHIELD_WIDTH, SHIELD_HEIGHT, Color.GREEN));
        elements.add(shields[3] = new Shields(spacing+SHIELD_WIDTH+spacing+SHIELD_WIDTH+spacing + SHIELD_WIDTH+spacing, HEIGHT-180, SHIELD_WIDTH, SHIELD_HEIGHT, Color.GREEN));
        
        new Thread(this).start();
    }
    
    
    public void run() {
        long tm = System.currentTimeMillis();           //get current time
        
        while (true) {
            repaint();
            update();
            endGame();
            
            
            //Walls against player collision
            if (outerWalls[0].isColliding(player)) {            //Player Colliding with Left wall
                player.setX(0);
            }
            if (outerWalls[1].isColliding(player)) {            //Player Colliding with Right wall
                player.setX(WIDTH-player.getWidth());
            }
            
            //missile against enemies collision
            attackEnemiesCollisionLoop: for (int row = 0; row < enemyRows.size(); row ++) {
                for (int col = 0; col < enemyRows.get(row).size(); col ++) {
                    for (int i = 0; i < missiles.size(); i ++) {
                        if (enemyRows.get(row).get(col).isColliding(missiles.get(i))) {
                            enemyRows.get(row).remove(col);
                            missiles.remove(i);
                            score += 100;
                            break attackEnemiesCollisionLoop;
                        }
                    }
                }
            }
            
            //shield against missile and enemyMissile collision
            for (int i = 0; i < shields.length; i ++) {
                for (int j = 0; j < missiles.size(); j ++) {
                    if (shields[i].isColliding(missiles.get(j))) {
                        missiles.remove(j);
                    }
                }
                
                for (int j = 0; j < enemyMissiles.size(); j ++) {
                    if (shields[i].isColliding(enemyMissiles.get(j))) {
                        enemyMissiles.remove(j);
                    }
                }
            }
            
            //missile against enemyMissile collision
            missileAgainstEnemyMissileCollisionLoop: for (int i = 0; i < missiles.size(); i ++) {
                for (int j = 0; j < enemyMissiles.size(); j ++) {
                    if (missiles.get(i).isColliding(enemyMissiles.get(j))) {
                        missiles.remove(i);
                        enemyMissiles.remove(j);
                        break missileAgainstEnemyMissileCollisionLoop;
                    }
                }
            }
            
            //enemyMissile against Player
            enemyMissileAgainstPlayerCollisionLoop: for (int i = 0; i < enemyMissiles.size(); i ++) {
                if (enemyMissiles.get(i).isColliding(player)) {
                    lives --;
                    enemyMissiles.remove(i);
                    endGame();
                    break enemyMissileAgainstPlayerCollisionLoop;
                }
            }
            
            
            //Toggle the Enemy Images
            if (timer%50 == 0) {
                for (int row = 0; row < enemyRows.size(); row ++) {
                    for (int col = 0; col < enemyRows.get(row).size(); col ++) {
                        enemyRows.get(row).get(col).toggleImage();
                    }
                }
            }
            
            
            
            
            try {
                tm += FPS;
                Thread.sleep(Math.max(0, tm - System.currentTimeMillis()));
                timer ++;
                enemyMissileTimer ++;
                bomberTimer --;
            } catch (InterruptedException e) {
                System.out.println(e);
            }
        }
    }
    
    public void paint(Graphics g) {
        Iterator iterator = elements.iterator();
        Iterator missileIterator = missiles.iterator();
        Iterator enemyMissileIterator = enemyMissiles.iterator();
        Iterator enemyRowsIterator;
        
        while (iterator.hasNext()) {
            ((Element) iterator.next()).draw(g);
        }
        while (missileIterator.hasNext()) {
            ((Element) missileIterator.next()).draw(g);
        }
        while (enemyMissileIterator.hasNext()) {
            ((Element) enemyMissileIterator.next()).draw(g);
        }
        for (int row = 0; row < enemyRows.size(); row ++) {
            enemyRowsIterator = enemyRows.get(row).iterator();
            while (enemyRowsIterator.hasNext()) {
                ((Element) enemyRowsIterator.next()).draw(g);
            }
        }
    }
    
    public void update() {
        //Enemy Update
        enemyMovementLoop: for (int row = 0; row < enemyRows.size(); row ++) {
            for (int col = 0; col < enemyRows.get(row).size(); col ++) {
                if (timer%2 == 0) {
                    enemyRows.get(row).get(col).update();
                }
                if (outerWalls[0].isColliding(enemyRows.get(row).get(col))) {       //first enemy is at left wall - move right
                    toggleAllEnemyMovements();
                    break enemyMovementLoop;
                }
                else if (outerWalls[1].isColliding(enemyRows.get(row).get(col))) {  //last enemy is at right wall - move left
                    toggleAllEnemyMovements();
                    break enemyMovementLoop;
                }
            }
        }
        
        
        //Missile Update
        for (int i = 0; i < missiles.size(); i ++) {
            missiles.get(i).setY(missiles.get(i).getY() - MISSILE_MOVE);
            if (missiles.get(i).getY() < (0-MISSILE_HEIGHT)) {              //Missile is fully off of window
                missiles.remove(i);
                i --;
            }
        }
        
        
        //EnemyMissile Update
        if (enemyMissileTimer >= nextEnemyFire) {
            enemyMissileTimer = 0;
            nextEnemyFire = getRandomNum(0, 200);
            enemyFire();
        }
        for (int i = 0; i < enemyMissiles.size(); i ++) {
            enemyMissiles.get(i).setY(enemyMissiles.get(i).getY() + MISSILE_MOVE);
            if (enemyMissiles.get(i).getY() + enemyMissiles.get(i).getHeight() >= HEIGHT) {
                enemyMissiles.remove(i);
                i --;
            }
        }
        
        
        //Player Update
        player.move(player.getMovement(), 0);
        
        
        //Shields Update
        for (int i = 0; i < shields.length; i ++) {
            shields[i].update();
        }
        
        
        //Bomber Update
        //Bomber needs to be placed
        
        //Bomber is already on the screen
        if (bomberTimer <= 0) {
            
        }
    }
    
    /**
     * get a random number between lowBound and upBound inclusive
     * @param lowBound - lower boundary
     * @param upBound - upper boundary
     * @return a random number between lowBound and upBound inclusively
     */
    public static int getRandomNum(int lowBound, int upBound) {
        if (lowBound < 0 || upBound < 0) {
            return 0;
        }
        Random rand = new Random();
        return rand.nextInt((upBound - lowBound) + 1) + lowBound;
    }
    
    public void enemyFire() {
        int totalEnemies = 0;
        for (int row = 0; row < enemyRows.size(); row ++) {
            for (int col = 0; col < enemyRows.get(row).size(); col ++) {
                totalEnemies ++;
            }
        }
        
        int targetEnemy;
        int targetEnemyRow = 0;         //Default to 0
        int targetEnemyCol = 0;         //Default to 0
        int count = 0;
        if (totalEnemies > 0) {
            targetEnemy = getRandomNum(0, totalEnemies);                //get a target enemy in the ArrayList<ArrayList>
            //find the enemy by counting through all the enemies
            for (int row = 0; row < enemyRows.size(); row ++) {
                for (int col = 0; col < enemyRows.get(row).size(); col ++) {
                    if (count == targetEnemy) {
                        targetEnemyRow = row;
                        targetEnemyCol = col;
                    }
                    count ++;
                }
            }
            if (targetEnemyRow > 0 && targetEnemyCol > 0) {
                enemyMissiles.add(new Rectangle(enemyRows.get(targetEnemyRow).get(targetEnemyCol).getX() + (CHARACTER_WIDTH /2),
                                                enemyRows.get(targetEnemyRow).get(targetEnemyCol).getY() + CHARACTER_HEIGHT,
                                                ENEMY_MISSILE_WIDTH, ENEMY_MISSILE_HEIGHT, Color.GREEN));
            }
        }
    }
    
    public void toggleAllEnemyMovements() {
        for (int row = 0; row < enemyRows.size(); row ++) {
            for (int col = 0; col < enemyRows.get(row).size(); col ++) {
                enemyRows.get(row).get(col).toggleMovement();
            }
        }
    }
    
    /**
     * Checks to see if Player has no lives left
     * If [lives] < 0 then end the game - go back to title menu
     */
    private void endGame() {
        if (lives < 0) {
            JOptionPane.showMessageDialog(null, "You are out of lives! Your score is: " + String.valueOf(score));
            System.exit(0);
        }
    }
    
    public keyListener getKeyListener() {
        return new keyListener();
    }
    
    public Player getPlayer() {
        return player;
    }
    
    
    
    
    
    
    
    //KeyEvents
    public static class keyListener extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent ke) {
            if (ke.getKeyCode() == KeyEvent.VK_ESCAPE) {
                System.exit(0);
            }
            
            //Launch Missile
            if (ke.getKeyCode() == KeyEvent.VK_SPACE) {
                if (missiles.size() < MAX_MISSILE_COUNT) {
                    int startX = player.getX() + (player.getWidth() / 2);
                    int startY = player.getY() - MISSILE_HEIGHT - 5;
                    missiles.add(new Rectangle(startX, startY, MISSILE_WIDTH, MISSILE_HEIGHT, Color.GREEN));
                }
            }
        }
    }
    
}
