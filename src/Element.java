
import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Nikhil Sinha sinhanr@mail.uc.edu
 */
public abstract class Element {
    
    //Global Variables
    private int x, y, width, height;
    private Color color = Color.WHITE;          //Default to white
    
    
    public Element(int newX, int newY, int newWidth, int newHeight, Color newColor) {
        x = newX;
        y = newY;
        width = newWidth;
        height = newHeight;
        color = newColor;
    }
    
    public Element (int newX, int newY, int newWidth, int newHeight) {
        x = newX;
        y = newY;
        width = newWidth;
        height = newHeight;
    }
    
    
    public abstract void draw(Graphics g);
    
    public boolean isColliding(Element e) {
        int leftSide = getX();
        int rightSide = getX() + getWidth();
        int top = getY();
        int bottom = getY() + getHeight();
        
        int eLeftSide = e.getX();
        int eRightSide = e.getX() + e.getWidth();
        int eTop = e.getY();
        int eBottom = e.getY() + e.getHeight();
        
        
        if (eLeftSide < rightSide) {
            if (eRightSide > leftSide) {
                if (eTop < bottom) {
                    if (eBottom > top) {
                        return true;
                    }
                }
            }
        }
        
        
        return false;
    }
    
    /**
     * get a random number between 0.0d(inclusive) and 1.0d(exclusive)
     * @return a random double between 0.0d(inclusive) and 1.0d(exclusive)
     */
    public double getRandDouble() {
        Random rnd = new Random();
        return rnd.nextDouble();
    }
    
    /**
     * get a random number between lowBound and upBound inclusive
     * @param lowBound - lower boundary
     * @param upBound - upper boundary
     * @return a random number between lowBound and upBound inclusively
     */
    public static int getRandInt(int lowBound, int upBound) {
        if (lowBound < 0 || upBound < 0) {
            return 0;
        }
        Random rand = new Random();
        return rand.nextInt((upBound - lowBound) + 1) + lowBound;
    }
    
    
    
    
    //Getters and Setters
    public int getX() {
        return x;
    }
    public void setX(int newX) {
        x = newX;
    }
    
    public int getY() {
        return y;
    }
    public void setY(int newY) {
        y = newY;
    }
    
    public int getWidth() {
        return width;
    }
    public void setWidth(int newWidth) {
        width = newWidth;
    }
    
    public int getHeight() {
        return height;
    }
    public void setHeight(int newHeight) {
        height = newHeight;
    }
    
    public Color getColor() {
        return color;
    }
    public void setColor(Color newColor) {
        color = newColor;
    }
    
}
