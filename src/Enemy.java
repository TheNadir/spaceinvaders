
import java.awt.Image;
import javax.swing.ImageIcon;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Nikhil Sinha sinhanr@mail.uc.edu
 */
public class Enemy extends Img {
    
    //Global Variables
    private final int MOVE = 1;
    private final int MOVE_DOWN = 20;
    private boolean movingRight = true;
    
    
    
    
    
    public Enemy(int x, int y, int width, int height, Image img) {
        super(x, y, width, height, img);
    }
    
    public Enemy(int x, int y, int width, int height, String imgPath) {
        super(x, y, width, height, imgPath);
    }
    
    
    
    public void update() {
        if (movingRight) {
            move(MOVE, 0);
        }
        else {
            move((-1) * MOVE, 0);
        }
    }
    
    public void move(int x, int y) {
        setX(getX() + x);
        setY(getY() + y);
    }
    
    public void toggleMovement() {
        movingRight = !movingRight;
        moveDown();
    }
    
    public void moveDown() {
        move(0, MOVE_DOWN);
        update();
    }
    
    public void toggleImage() {
        ImageIcon ii = new ImageIcon(this.getClass().getResource("images/Enemy1.1.png"));
        Image img = ii.getImage();
        if (getImage().equals(img)) {
            setImageByPath("images/Enemy1.2.png");
            return;
        }
        ii = new ImageIcon(this.getClass().getResource("images/Enemy1.2.png"));
        img = ii.getImage();
        if (getImage().equals(img)) {
            setImageByPath("images/Enemy1.1.png");
            return;
        }
        
        ii = new ImageIcon(this.getClass().getResource("images/Enemy2.1.png"));
        img = ii.getImage();
        if (getImage().equals(img)) {
            setImageByPath("images/Enemy2.2.png");
            return;
        }
        
        ii = new ImageIcon(this.getClass().getResource("images/Enemy2.2.png"));
        img = ii.getImage();
        if (getImage().equals(img)) {
            setImageByPath("images/Enemy2.1.png");
            return;
        }
        
        ii = new ImageIcon(this.getClass().getResource("images/Enemy3.1.png"));
        img = ii.getImage();
        if (getImage().equals(img)) {
            setImageByPath("images/Enemy3.2.png");
            return;
        }
        
        ii = new ImageIcon(this.getClass().getResource("images/Enemy3.2.png"));
        img = ii.getImage();
        if (getImage().equals(img)) {
            setImageByPath("images/Enemy3.1.png");
            return;
        }
    }
    
}
