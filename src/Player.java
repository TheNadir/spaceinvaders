
import java.awt.Image;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Nikhil Sinha sinhanr@mail.uc.edu
 */
public class Player extends Img {
    
    //Global Variables
    private final int MOVE = 6;
    private int movement = 0;               //0 = Not moving; 1 = moving Left; 2 = moving Right
    
    public final int NOT_MOVING = 0;
    public final int MOVING_LEFT = (-1) * MOVE;
    public final int MOVING_RIGHT = MOVE;
    
    
    
    
    public Player(int x, int y, int width, int height, Image img) {
        super(x, y, width, height, img);
    }
    
    public Player(int x, int y, int width, int height, String imgPath) {
        super(x, y, width, height, imgPath);
    }
    
    
    
    
    public playerKeyListener getListener() {
        return new playerKeyListener();
    }
    
    public void move(int x, int y) {
        setX(getX() + x);
        setY(getY() + y);
    }
    
    public int getMovement() {
        return movement;
    }
    
    public void setMoving(int newMovement) {
        movement = newMovement;
    }
    
    
    
    
    
    
    
    
    
    public class playerKeyListener extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent ke) {
            if (ke.getKeyCode() == KeyEvent.VK_RIGHT || ke.getKeyCode() == KeyEvent.VK_D) {
                //setX(getX() + MOVE);
                setMoving(MOVING_RIGHT);
            }
            if (ke.getKeyCode() == KeyEvent.VK_LEFT || ke.getKeyCode() == KeyEvent.VK_A) {
                //setX(getX() - MOVE);
                setMoving(MOVING_LEFT);
            }
        }
        
        @Override
        public void keyReleased(KeyEvent ke) {
            if (ke.getKeyCode() == KeyEvent.VK_RIGHT || ke.getKeyCode() == KeyEvent.VK_D) {
                setMoving(NOT_MOVING);
            }
            if (ke.getKeyCode() == KeyEvent.VK_LEFT || ke.getKeyCode() == KeyEvent.VK_A) {
                setMoving(NOT_MOVING);
            }
        }
    }
}
